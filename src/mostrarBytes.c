#include <stdio.h>
#include <stdlib.h>
#include "cabecera.h"

void mostrarBytes(void *valor, unsigned int tam){

	printf("%p 	", valor);
	for (int i = 0; i<tam; ++i)
	{
		printf("%02x    ", *(unsigned char*)(valor+i));
	}
	printf("\n");
}