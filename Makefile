CC=gcc -Wall -c
IC=-I include/
FS=-fsanitize=address,undefined
bin/taller: obj/mostrarBytes.o obj/main.o  
	mkdir -p bin/
	gcc $^ -o bin/taller $(FS)

obj/main.o: src/main.c
	$(CC) $(IC)  src/main.c -o obj/main.o $(FS)

obj/mostrarBytes.o: src/mostrarBytes.c
	mkdir -p obj/
	$(CC) $(IC) src/mostrarBytes.c -o obj/mostrarBytes.o $(FS)

.Phony: clean
clean: 
	rm obj/* bin/*
